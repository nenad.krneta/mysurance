import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import InsuranceOverview from '../Mysurance/InsuranceOverview/InsuranceOverview';
import AddNewInsurance from '../Mysurance/AddNewInsurance/AddNewInsurance';


class Mysurance extends Component {
    render () {
        return (
                <Switch>
                    <Route path="/new-insurance" component={AddNewInsurance} />
                    <Route path="/overview" exact component={InsuranceOverview} />
                    <Redirect from="/" to="/overview" />
                </Switch>
        );
    }
}

export default Mysurance;