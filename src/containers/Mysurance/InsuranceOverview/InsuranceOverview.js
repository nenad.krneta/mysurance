import React, { Component } from 'react';
import InsuranceList from '../../../components/InsuranceList/InsuranceList';
import InsurancePremiumSummary from '../../../components/InsurancePremiumSummary/InsurancePremiumSummary';
import InsuranceModal from '../../../components/InsuranceModal/InsuranceModal';
import insuranceCategoriesService from '../../../services/InsuranceCategories/InsuranceCategoriesService';
import insurancesService from '../../../services/Insurances/InsurancesService';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import './InsuranceOverview.css';

export class InsuranceOverview extends Component {
    state = {
        insuranceItems: [],
        insuranceCategories: [],
        modalIsOpen: false,
        selectedItem: {},
        loading: false,
        deleting: false
    }

    loadInsuranceItems = async () => {
        this.setState({ loading: true });
        const [insurances, categories] = await Promise.all([insurancesService.getAll(), insuranceCategoriesService.getAll()]);

        const categoriesResponse = categories.data.query.categorymembers;
        const insuranceListResponse = insurances.data || {};

        const categoriesData = categoriesResponse.reduce((obj, item) => {
            obj[item.pageid] = item.title.replace('Category:', '');
            return obj;
        }, {});

        const insuranceItems = Object.entries(insuranceListResponse).map(([id, value]) => {
            return {
                id,
                insuranceCategoryId: value.insuranceCategory,
                insuranceCategory: categoriesData[value.insuranceCategory],
                title: value.title,
                yearlyPremium: value.yearlyPremium
            }
        });

        this.setState({
            insuranceItems: insuranceItems,
            insuranceCategories: categoriesData,
            loading: false
        });
    }

    componentDidMount() {
        this.loadInsuranceItems();
    }

    newInsuranceHandler = () => {
        this.props.history.push('/new-insurance');
    }

    insuranceItemClickHandler = (e, item) => {
        this.setState({ modalIsOpen: true, selectedItem: item });
    }

    deleteInsuranceItem = () => {
        this.setState({ deleting: true });
        insurancesService.delete(this.state.selectedItem.id)
            .then(res => {
                this.loadInsuranceItems();
                this.setState({ modalIsOpen: false, selectedItem: {}, deleting: false });
            })
            .catch(error => {
                this.setState({ modalIsOpen: false, selectedItem: {}, deleting: false });
            });
    }

    closeModal = () => {
        this.setState({ modalIsOpen: false, selectedItem: {} });
    }

    render() {
        return (
            <div className="insuranceOverview container">
                <div className="appTitle">
                    Mysurance
                </div>
                <InsurancePremiumSummary insuranceItems={this.state.insuranceItems} />
                <InsuranceList
                    insuranceItems={this.state.insuranceItems}
                    insuranceItemClickHandler={this.insuranceItemClickHandler}
                    newInsuranceHandler={this.newInsuranceHandler}
                />
                <InsuranceModal
                    modalIsOpen={this.state.modalIsOpen}
                    closeModal={this.closeModal}
                    selectedItem={this.state.selectedItem}
                    deleteInsuranceItem={this.deleteInsuranceItem}
                    showSpinner={this.state.deleting}
                />
            </div>
        );
    }
}

export default withErrorHandler(InsuranceOverview, [insurancesService.axiosInstance, insuranceCategoriesService.axiosInstance]);
