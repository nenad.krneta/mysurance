import React, { Component } from 'react';
import insuranceCategoriesService from '../../../services/InsuranceCategories/InsuranceCategoriesService';
import insurancesService from '../../../services/Insurances/InsurancesService';
import NewInsuranceForm from '../../../components/NewInsuranceForm/NewInsuranceForm';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import './AddNewInsurance.css';

export class AddNewInsurance extends Component {
    state = {
        loading: false,
        newInsuranceForm: {
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text'
                },
                placeholder: 'Title',
                value: '',
                validation: {
                    required: true
                },
                isValid: false,
                touched: false
            },
            yearlyPremium: {
                elementType: 'float',
                elementConfig: {
                    type: 'float'
                },
                placeholder: 'Yearly premium',
                value: '',
                validation: {
                    required: true,
                    float: true
                },
                isValid: false,
                touched: false
            },
            insuranceCategory: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                placeholder: 'Insurance category',
                value: '',
                isValid: true,
                touched: false
            },
        },
        formValid: false
    }

    setCategoryOptions(categoriesResponse) {
        const categoriesData = categoriesResponse.map(category => {
            return {
                value: category.pageid,
                displayValue: category.title.replace('Category:', '')
            }
        });

        const form = { ...this.state.newInsuranceForm };
        const categoriesState = { ...form['insuranceCategory'] };
        categoriesState.elementConfig.options = categoriesData;

        categoriesState.value = categoriesData[0].value;
        form['insuranceCategory'] = categoriesState;

        return form;
    }

    componentDidMount = async () => {
        this.setState({ loading: true });
        const { data: { query: { categorymembers } } } = await insuranceCategoriesService.getAll();
        const form = this.setCategoryOptions(categorymembers);
        this.setState({
            newInsuranceForm: form,
            loading: false
        });
    }

    cancelHandler = (e) => {
        this.props.history.push('/overview');
    };

    submitFormHandler = (e) => {
        e.preventDefault();
        const formState = this.state.newInsuranceForm;
        let formData = {};
        Object.keys(formState).forEach(formElement => {
            formData[formElement] = formState[formElement].value;
        });

        this.addInsurance(formData);
    }

    addInsurance = async (data) => {
        this.setState({ loading: true });
        await insurancesService.add(data);
        this.props.history.push('/overview');
    }

    checkInputValidity(value, rules) {
        let isValid = true;

        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = (rules.float ? value !== 0 && value !== '' : value.trim() !== '') && isValid;
        }

        if (rules.float) {
            isValid = !isNaN(parseFloat(value)) && isValid;
        }

        return isValid;
    }

    inputChangedHandler = (e, inputIdentifier) => {
        const updatedForm = { ...this.state.newInsuranceForm };
        const updatedFormElement = { ...updatedForm[inputIdentifier] };
        updatedFormElement.value = updatedFormElement.elementType === 'float' ? e.floatValue || e.formattedValue : e.target.value;
        updatedFormElement.isValid = this.checkInputValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formValid = true;
        for (let formEl in updatedForm) {
            formValid = updatedForm[formEl].isValid && formValid;
        }

        this.setState({
            newInsuranceForm: updatedForm,
            formValid: formValid
        });
    }

    getFormElementsArray() {
        return Object.entries(this.state.newInsuranceForm).map(([key, value]) => {
            return {
                id: key,
                config: value
            }
        });
    }

    render() {
        return (
            <div className="container addNewInsurance">
                <div className="addNewInsurance">
                    <div className="appTitle">
                        Mysurance
                    </div>
                    <h3 className="text-center">Add new insurance</h3>
                    <NewInsuranceForm
                        submitForm={this.submitFormHandler}
                        formElementsArray={this.getFormElementsArray()}
                        inputChangedHandler={this.inputChangedHandler}
                        cancelHandler={this.cancelHandler}
                        formValid={this.state.formValid}
                        showSpinner={this.state.loading}
                    />
                </div>
            </div>
        );
    }
}

export default withErrorHandler(AddNewInsurance, [insuranceCategoriesService.axiosInstance, insurancesService.axiosInstance]);