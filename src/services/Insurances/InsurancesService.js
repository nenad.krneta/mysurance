import axios from "../../axiosConfig";

const insurancesService = {
		axiosInstance: axios,
		getAll: () => axios.get("/insurances.json"),
		add: insurance => axios.post("/insurances.json", insurance),
		delete: insuranceId => axios.delete(`/insurances/${insuranceId}.json`)
}

export default insurancesService;

