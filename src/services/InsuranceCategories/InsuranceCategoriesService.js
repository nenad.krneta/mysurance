import axios from "../../axiosConfig";

const insuranceCategoriesService = {
		axiosInstance: axios,
		getAll: () => axios.get("https://en.wikipedia.org/w/api.php?action=query&list=categorymembers&cmtitle=Category:Types_of_insurance&cmtype=subcat&format=json&origin=*")
}

export default insuranceCategoriesService;

