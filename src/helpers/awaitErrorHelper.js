export default function awaitErrorHelper(promise) {
    return promise.then(data => {
       return [null, data];
    })
    .catch(err => [err]);
 }