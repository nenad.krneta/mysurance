import React, {Component} from 'react';
import Modal from '../../components/UI/Modal/Modal';

const withErrorHandler = (WrappedComponent, axiosInstances) => {

    return class extends Component {
        state = {
            error: null
        }

        reqInterceptor = [];
        resInterceptor = [];

        componentWillMount(){
            axiosInstances.forEach((element, index) => {
                this.reqInterceptor[index] = element.interceptors.request.use(req => {
                    this.setState({error: null});
                    return req;
                });

                this.resInterceptor[index] = element.interceptors.response.use(res => res, error => {
                    this.setState({error: error});
                    throw error;
                });
            });
        }

        errorConfirmedHandler = () => {
            this.setState({error: null});
        }

        componentWillUnmount() {
            axiosInstances.forEach(function(element, index){
                element.interceptors.request.eject(this.reqInterceptor[index]);
                element.interceptors.response.eject(this.resInterceptor[index]);
            }.bind(this));
        }

        render(){
            return (
                <div>
                    <Modal show={this.state.error} modalClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message: null}
                    </Modal>
                    <WrappedComponent {...this.props} />
                </div>
            )
        }
    }
}

export default withErrorHandler;