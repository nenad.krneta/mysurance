import InsuranceCategoriesService from '../../services/InsuranceCategories/InsuranceCategoriesService';

describe('Insurance categories service', () => {

    it('should get more than one category', () => {
        InsuranceCategoriesService.getAll()
            .then(response => {
                expect(response.data.query.categorymembers.length).not.toBeLessThan(1);
            });

    });
})