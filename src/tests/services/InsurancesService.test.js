import InsurancesService from '../../services/Insurances/InsurancesService';

describe('Insurances service', () => {
    it('should return response with status code 200', () => {
        InsurancesService.getAll()
            .then(response => {
                expect(response.status).toEqual(200);
            });
    });
})