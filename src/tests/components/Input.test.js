import React from 'react';
import { shallow, mount } from 'enzyme';
import Input from '../../components/UI/Input/Input';

describe('Input component', () => {

    it('renders without crashing', () => {
        const wrapper = shallow(<Input />);
        expect(wrapper).toHaveLength(1);
    });

    it('should render input element', () => {
        const inputConfig =  {
            type: "text"
        };

        const inputComponent = shallow(<Input elementType="input" elementConfig={inputConfig} />);
        expect(inputComponent.find('input').length).not.toBeLessThan(1);
    });

    it('should render select element with two options', () => {
        const inputConfig =  {
            options: [
                {
                    value: 'foo',
                    displayValue:'Foo'
                },
                {
                    value: 'bar',
                    displayValue:'Bar'
                }
            ]
        };

        const inputComponent = shallow(<Input elementType={"select"} elementConfig={inputConfig}  />);
        expect(inputComponent.find('select>option').length).not.toBeLessThan(2);
    });

})