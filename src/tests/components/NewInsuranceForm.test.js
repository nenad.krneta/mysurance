import React from 'react';
import { shallow, mount } from 'enzyme';
import NewInsuranceForm from '../../components/NewInsuranceForm/NewInsuranceForm';

const formElements = [
  {
    id: "title",
    config: {
      elementType: "input",
      elementConfig: {
        "type": "text"
      },
      placeholder: "Title",
      value: "",
      validation: {
        required: true
      },
      isValid: false,
      touched: false
    }
  },
  {
    id: "yearlyPremium",
    config: {
      elementType: "float",
      elementConfig: {
        "type": "float"
      },
      placeholder: "Yearly premium",
      value: "",
      validation: {
        required: true,
        float: true
      },
      isValid: false,
      touched: false
    }
  }
];

describe('Form component', () => {

  it('should render form element with 2 input fields', () => {

    const formComponent = shallow(<NewInsuranceForm
      formElementsArray={formElements}
      formValid={true}
      showSpinner={false} />);

    expect(formComponent.find('input').length).toBe(2);
  });

  it('should render form element enabled submit button', () => {

    const formComponent = shallow(<NewInsuranceForm
      formElementsArray={formElements}
      formValid={true}
      showSpinner={false} />);

    expect(formComponent.find('button.done').props().disabled).toBe(false);
  });

  // it('validation should return false if no value is provided for mandatory form field', () => {

  //   const addNewInsurance = shallow(<AddNewInsurance />);
  //   const instance = addNewInsurance.instance();

  //   expect(instance.checkInputValidity('', {
  //     required: true
  //   })).toBe(false);
  // });

  // it('should fail if submit button is not disabled on component load', () => {
  //   const addNewInsurance = mount(<AddNewInsurance />);
  //   const instance = addNewInsurance.instance();
  //   instance.getInsuranceCategories().then(() => {
  //     addNewInsurance.update();
  //     expect(addNewInsurance.find('button.done').props().disabled).toBe(true);
  //   });
  // });
})