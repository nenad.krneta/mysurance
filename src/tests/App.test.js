import React from 'react';
import { shallow, mount } from 'enzyme'
import App from '../App';

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

it('renders without crashing', () => {
	const wrapper = shallow(<App />);
	expect(wrapper).toHaveLength(1);
});




