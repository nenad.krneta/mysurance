import React from 'react';
import { shallow, mount } from 'enzyme'
import {AddNewInsurance} from '../../containers/Mysurance/AddNewInsurance/AddNewInsurance';

import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('Add new insurance', () => {

    it('renders without crashing', () => {
        const wrapper = shallow(<AddNewInsurance />);
        expect(wrapper).toHaveLength(1);
    });

    it('validation should return false for if text is eneterd for float type', () => {

        const addNewInsurance = shallow(<AddNewInsurance />);
        const instance = addNewInsurance.instance();

        expect(instance.checkInputValidity('test', {
            float: true
        })).toBe(false);
    });
})