import React from 'react';
import { shallow, mount } from 'enzyme'
import { InsuranceOverview } from '../../containers/Mysurance/InsuranceOverview/InsuranceOverview';
import Insurance from '../../components/Insurance/Insurance';
import axios from '../../axiosConfig';


import jsdom from 'jsdom';
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('Insurance Overview', () => {
    it('renders without crashing', () => {
        const wrapper = shallow(<InsuranceOverview />);
        expect(wrapper).toHaveLength(1);
    });

    it('should load insurance categories before rendering', () => {
        expect.assertions(1);
        const overview = mount(<InsuranceOverview />);

        overview.setState({
            insuranceItems:
                [
                    {
                        "id": "-LOKP_Zdq2SCKL9Hvi5y",
                        "insuranceCategoryId": "55176392",
                        "insuranceCategory": "Flood insurance",
                        "title": "Travel",
                        "yearlyPremium": 54.9
                    },
                    {
                        "id": "-LOUoRMs_m9DdJUcEk7H",
                        "insuranceCategoryId": "51782206",
                        "insuranceCategory": "Property insurance",
                        "title": "Car",
                        "yearlyPremium": 345.7
                    },
                    {
                        "id": "-LOUo_WWkK6oYhiYrsUZ",
                        "insuranceCategoryId": "51782206",
                        "insuranceCategory": "Property insurance",
                        "title": "House",
                        "yearlyPremium": 234.8
                    }
                ]
        });

        expect(overview.find(Insurance).length).not.toBeLessThan(3);
    });

});