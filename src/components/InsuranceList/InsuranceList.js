import React from 'react';
import Insurance from '../Insurance/Insurance';

const InsuranceList = ({insuranceItems, newInsuranceHandler, insuranceItemClickHandler}) => {
    if (!insuranceItems.length) {
        return <div>
            <div className="noItems">Please add new insurance item.</div>
            <div className="floating-button" onClick={newInsuranceHandler}>
                <p className="plus">+</p>
            </div>
        </div>;
    }

    return (
        <div>
            {
                <div className="collection">
                    {insuranceItems.map(item => (
                        <Insurance
                            key={item.id}
                            clickHandler={(event) => insuranceItemClickHandler(event, item)}
                            title={item.title}
                            yearlyPremium={item.yearlyPremium}
                            insuranceCategory={item.insuranceCategory} />
                    ))
                    }
                    <div className="floating-button" onClick={newInsuranceHandler}>
                        <p className="plus">+</p>
                    </div>
                </div>
            }
        </div>
    )
}

export default InsuranceList;
