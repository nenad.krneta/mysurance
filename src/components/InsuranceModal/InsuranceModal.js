import React from 'react';
import Modal from '../UI/Modal/Modal';
import { BeatLoader } from 'react-spinners';
import Insurance from '../Insurance/Insurance';

const InsuranceModal = ({selectedItem, closeModal, deleteInsuranceItem, showSpinner, modalIsOpen}) => {
    let modalContent = (
        <div>
            <Insurance
                insuranceClass="modalInsurance"
                key={selectedItem.insuranceCategoryId}
                title={selectedItem.title}
                yearlyPremium={selectedItem.yearlyPremium}
                insuranceCategory={selectedItem.insuranceCategory} />
            <div className="modalMessage">Do you want to delete this insurance item?</div>
            <div className="button-container">
                <button className="btn-materialize cancel" onClick={closeModal}>No</button>
                <button className="btn-materialize delete" onClick={deleteInsuranceItem}>Yes</button>
            </div>
        </div>
    );
    let closeHandler = closeModal;
    if (showSpinner) {
        closeHandler = function(){};
        modalContent = (
            <BeatLoader
                className="spinnerOverride"
                sizeUnit={"px"}
                size={15}
                color={'#36D7B7'}
                loading={showSpinner}
            />
        );
    }
    return (
        <Modal
            show={modalIsOpen}
            modalClosed={closeHandler}>
            {modalContent}
        </Modal>
    )
}

export default InsuranceModal;
