import React from 'react'
import NumberFormat from 'react-number-format';
import './Input.css';

const input = ({invalid, shouldValidate, touched, elementType, changedHandler, elementConfig, value, label}) => {
    let inputElement = null;
    const inputClasses = [''];

    if (invalid && shouldValidate && touched) {
        inputClasses.push('invalid')
    }

    switch (elementType) {
        case ('input'):
            inputElement = <input
                onChange={changedHandler}
                className={inputClasses.join(' ')}
                {...elementConfig}
                value={value} />;
            break;
        case ('float'):
            inputElement = <NumberFormat
                value={value}
                thousandSeparator={true}
                onValueChange={changedHandler}
                prefix={'€ '}
                className={inputClasses.join(' ')}/>;
            break;
        case ('select'):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    onChange={changedHandler}
                    value={value}>
                    {elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
            );
            break;
        default:
            inputElement = <input className={inputClasses.join(' ')} />;
            break;
    }
    let validationError = null;
    if (invalid && touched) {
        validationError = <p className="validationError">Please enter a valid value!</p>;
    }
    return (
        <div className="input-field col s12">
            {inputElement}
            <label className="active">{label}</label>
            {validationError}
        </div>
    )
}

export default input;
