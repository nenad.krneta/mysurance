import React from 'react';
import NumberFormat from 'react-number-format';
import {sumBy}  from 'lodash';

const InsurancePremiumSummary = ({insuranceItems}) => {
    return (
        <div className="yearlyPremiumSum">
            <span>Yearly premiums sum</span>
            <NumberFormat
                value={sumBy(insuranceItems, 'yearlyPremium')}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'€ '}
                className="sum" />
        </div>
    )
}

export default InsurancePremiumSummary;
