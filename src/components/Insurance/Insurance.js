import React from 'react';
import NumberFormat from 'react-number-format';
import './Insurance.css';

const insurance = ({insuranceClass, clickHandler, yearlyPremium, title, insuranceCategory}) => {
    return (
        <div className={insuranceClass}>
            <a className="collection-item pointer" onClick={clickHandler}>
                <NumberFormat
                    value={yearlyPremium}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'€ '}
                    className="badge" />
                <div className="flex flex-middle">
                    <div className="circle text-center teal margin-right">{title ? title.substring(0, 1) : ''}</div>
                    <div>
                        <span className="capitilize-text">{title}</span><br />
                        <span className="description-text">{insuranceCategory}</span>
                    </div>
                </div>
            </a>
        </div>
    )
}

export default insurance;
