import React from 'react';
import Input from '../UI/Input/Input';
import { BeatLoader } from 'react-spinners';

const NewInsuranceForm = ({showSpinner, submitForm, formElementsArray, inputChangedHandler, cancelHandler, formValid}) => {
    if (showSpinner) {
        return <BeatLoader
            className="spinnerOverride"
            sizeUnit={"px"}
            size={15}
            color={'#36D7B7'}
            loading={showSpinner}
        />;
    }
    return (
        <form onSubmit={submitForm} className="addForm">
            {formElementsArray.map(formElement => (
                <Input
                    key={formElement.id}
                    label={formElement.config.placeholder}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    invalid={!formElement.config.isValid}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    changedHandler={(event) => inputChangedHandler(event, formElement.id)} />
            ))}
            <button className="btn-materialize cancel" onClick={cancelHandler}>Cancel</button>
            <button className="btn-materialize done" disabled={!formValid}>Done</button>
        </form>
    )
}

export default NewInsuranceForm;
