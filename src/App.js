import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Mysurance from './containers/Mysurance/Mysurance';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Mysurance />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
